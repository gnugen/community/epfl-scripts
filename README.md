# EPFL Scripts

Scripts that are useful when you spend time at EPFL.


## Installation

After installing the dependencies (see below), the various script can either
be installed somewhere on your system or run directly from the source tree.

If you want to install them (by copying them or creating symlinks), you can
use the [`install.sh`](install.sh) script.

Arch Linux users can also simply install
[`epfl-scripts-git`](https://aur.archlinux.org/packages/epfl-scripts-git/) from
the AUR.


## Dependencies

For the shell scripts, the following software must be installed on your system:

  * bash
  * coreutils
  * curl
  * file
  * openconnect
  * vpnc/vpnc-script

The network namespace jail feature in `epfl-vpn` additionally requires

  * iproute2
  * iptables

For the Perl scripts, the following modules must be installed on your system:

  * File::MimeInfo::Magic
  * HTML::TreeBuilder::XPath
  * HTML::TreeBuilder
  * IO::Scalar
  * LWP::UserAgent
  * Term::ANSIColor
  * WWW::Mechanize

For Debian (or Debian-based distributions like Ubuntu), the Perl modules are
provided by the following packages:

  * libfile-mimeinfo-perl
  * libhtml-treebuilder-xpath-perl
  * libhtml-tree-perl
  * libio-stringy-perl
  * libwww-perl
  * perl-modules
  * libwww-mechanize-perl


## Contributing

Pushing directly to the master branch is not permitted for reasons of
robustness. To contribute, please make your changes either in a separate branch
or in a fork, then open a merge request. This way, your changes can undergo a
brief review and feedback cycle before being applied.
