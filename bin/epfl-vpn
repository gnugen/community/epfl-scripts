#!/usr/bin/env bash
# Copyright 2015 Florian Vessaz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

# The base name of the application
BASENAME="$(basename "$0")"

# vpnc-script name
VPNC_SCRIPT_NAME="vpnc-script"

# Colon-separated list of paths where $VPNC_SCRIPT will be searched
VPNC_SCRIPT_PATH="/usr/share/vpnc-scripts:/etc/vpnc"

# Name of openconnect executable (will be searched in $PATH)
OPENCONNECT_NAME="openconnect"

# Hostname of the VPN server
VPNSERVER="vpn.epfl.ch"

## Jail-related variables --------------
# Target Network namespace name
NETNS_NAME="EpflVPN"
# local interface name
NETNS_LI="ToEPFL"
# Remote interface name
NETNS_RI="FromEPFL"
# Local Interface ip addr for jailed mode
NETNS_LIP=169.254.97.1/24
# Remote Interface ip addr for jailed mode
NETNS_RIP=169.254.97.172/24
# --------------------------------------


# Error codes
E_USER=1
E_NOCMD=127

die()
{
	retval="$1"; shift
	if [ $# -eq 0 ]; then
		cat <&0 >&2
	else
		# shellcheck disable=SC2059
		printf "$@" >&2; echo >&2
	fi
	if [ "$retval" = $E_USER ]; then
		printf "Run with --help for more information.\n" >&2
	fi
	exit "$retval"
}

usage()
{
	cat <<-EOF
	$BASENAME: Connect to the EPFL VPN

	Unless specified, only packets with a destination in the EPFL subnet are
	sent through the tunnel.

	Usage:
	$BASENAME [-h|-f]

	$BASENAME -j <user> <command>

	Options:
	  -h, --help
	        Show this help message and exit.

	  -f, --full
	        Send all packets through the VPN tunnel.

	  -j, --jailed <user> <command>
	        execute <command> as user <user> in a separate network namespace.
	        All of <command> packages will go through the VPN tunnel.

	Example:
	    $BASENAME -j $(whoami) gnome-terminal
	            Will spawn a terminal where all commands will go through the vpn.
	
	    $BASENAME -j $(whoami) firefox -P --no-remote
	            Will launch firefox with all it's traffic going through the vpn.
	
	EPFL subnets:
	  128.178.0.0/16
	  128.179.0.0/16
	EOF
}

check_user()
{
	if [ "$(id -u)" != '0' ]; then
		cat >&2 <<- EOF
		WARNING: Running as unprivileged user \`$(whoami)'.
		If the network setup fails, run this script as a more privileged user (e.g. root).
		EOF
	fi
}

check_openconnect()
{
	if ! OPENCONNECT="$(command -v "$OPENCONNECT_NAME")"; then
		die $E_NOCMD <<- EOF
		ERROR: The \`$OPENCONNECT_NAME' command does not appear to exist on this system.
		Please make sure that OpenConnect is installed.
		EOF
	fi
	printf "Using %s as %s.\n" "$OPENCONNECT" "$OPENCONNECT_NAME"
}

check_vpnc_script()
{
	if ! VPNC_SCRIPT="$(PATH="$VPNC_SCRIPT_PATH" command -v "$VPNC_SCRIPT_NAME")"; then
		die $E_NOCMD <<- EOF
		ERROR: $VPNC_SCRIPT_NAME was not found in the following colon-delimited paths:
		$VPNC_SCRIPT_PATH
		Please make sure that the distribution package providing $VPNC_SCRIPT_NAME is installed.
		EOF
	fi
	printf "Using %s as %s.\n" "$VPNC_SCRIPT" "$VPNC_SCRIPT_NAME"
}

check_path_bins()
{
	if ! command -v "$1" &> /dev/null; then
		die $E_NOCMD <<- EOF
		ERROR: "$1" was not found in \$PATH:
		Please make sure that the distribution package providing "$1" is installed.
		EOF
	fi
}

setup_network_jail(){
	echo 1 > /proc/sys/net/ipv4/ip_forward

	iptables -t nat -A POSTROUTING -s ${NETNS_RIP} -o "$OUTGOING_INTERFACE" -j MASQUERADE
	iptables -t filter -A FORWARD -i ${NETNS_LI} -j ACCEPT
	iptables -t filter -A FORWARD -o ${NETNS_LI} -j ACCEPT
	# Don't try to create the NNS if it exists.
	if ! ip netns list | grep "${NETNS_NAME}"; then
		# Setup NS, create and configure interfaces
		ip netns add ${NETNS_NAME}
	else
		echo "Detected previous namespace, recycling..."
	fi

	# Don't touch networking between the NNS and the host if the link exists.
	if ! ip link show ${NETNS_LI} 2> /dev/null; then
		ip link add ${NETNS_LI} type veth peer name ${NETNS_RI}
		ip link set ${NETNS_RI} netns ${NETNS_NAME}
		# Bring local interface to life
		ip addr add ${NETNS_LIP} dev ${NETNS_LI}
		ip link set ${NETNS_LI} up
		# Configure network jail and remote interface
		ip netns exec ${NETNS_NAME} ip link set ${NETNS_RI} up
		ip netns exec ${NETNS_NAME} ip addr add ${NETNS_RIP} dev ${NETNS_RI}
		ip netns exec ${NETNS_NAME} ip route add default via ${NETNS_LIP%/*} dev ${NETNS_RI}
	else
		echo "Detected previous NNS link. Skipped cross link configuration."
	fi
}

destroy_network_jail(){
	# Don't die on error
	set +e
	# Cleaning up Network Jail
	echo "Asking remaining processes to exit"
	# Be nice and send SIGINT to the processes inside the jail
	for pid in $(ip netns pids $NETNS_NAME); do
		kill -INT "$pid"
	done
	# They got 2 seconds to clear out!
	[ -z "$(ip netns pids $NETNS_NAME)" ] || sleep 2
	# Cleanup network jail and kill remote interface
	ip netns exec ${NETNS_NAME} ip route del default via ${NETNS_LIP%/*} dev ${NETNS_RI}
	ip netns exec ${NETNS_NAME} ip addr del ${NETNS_RIP} dev ${NETNS_RI}
	ip netns exec ${NETNS_NAME} ip link set ${NETNS_RI} down
	# Kill the remaining processes in the netns
	for pid in $(ip netns pids $NETNS_NAME); do
		kill -KILL "$pid"
	done
	[ -z "$(ip netns pids $NETNS_NAME)" ] || sleep 1
	# Cleanup local
	echo "Cleaning Network Jail"
	ip link del ${NETNS_LI}
	ip netns del ${NETNS_NAME}
	iptables -t nat -D POSTROUTING -s ${NETNS_RIP} -o "$OUTGOING_INTERFACE" -j MASQUERADE
	iptables -t filter -D FORWARD -i ${NETNS_LI} -j ACCEPT
	iptables -t filter -D FORWARD -o ${NETNS_LI} -j ACCEPT
}

_connect() { "$OPENCONNECT" "$VPNSERVER" "$@"; }
connect_full() { _connect; }
connect_split () { _connect --script="$(readlink -e "$0")"; }

connect_jailed(){
	check_path_bins iptables
	check_path_bins ip

	EPFL_IP_ADDR="$(getent ahostsv6 epfl.ch | head -n 1 | awk '{print $1}')"

	if ! ip route get "$EPFL_IP_ADDR" &> /dev/null; then
		# Fallbackk to IPv4 if we cannot route using IPv6
		EPFL_IP_ADDR="$(getent ahostsv4 epfl.ch | head -n 1 | awk '{print $1}')"
	fi

	OUTGOING_INTERFACE="$(ip route get "$EPFL_IP_ADDR" | grep -oP 'dev \K[^ ]+')"

	EPFL_SCRIPT="$0"
	setup_network_jail

	trap destroy_network_jail SIGINT SIGTERM
	export NETNS_NAME RUNAS_USER COMMAND
	ip netns exec ${NETNS_NAME} "${EPFL_SCRIPT}"
}

# If the script is called by openconnect, act as vpnc-script (for split mode):
if [ -n "$VPNGATEWAY" ]; then
	check_vpnc_script

	# If we're running in a jail, run the command
	if [ -n "$NETNS_NAME" ] && [ -n "$RUNAS_USER" ]; then
		# Kind of a hack, but I still want vpnc-script to configure routing for me.
		export CISCO_SPLIT_INC="1"
		export CISCO_SPLIT_INC_0_ADDR="0.0.0.0"
		export CISCO_SPLIT_INC_0_MASK="0.0.0.0"
		export CISCO_SPLIT_INC_0_MASKLEN="0"
	else
		export CISCO_SPLIT_INC="2"

		export CISCO_SPLIT_INC_0_ADDR="128.178.0.0"
		export CISCO_SPLIT_INC_0_MASK="255.255.0.0"
		export CISCO_SPLIT_INC_0_MASKLEN="16"

		export CISCO_SPLIT_INC_1_ADDR="128.179.0.0"
		export CISCO_SPLIT_INC_1_MASK="255.255.0.0"
		export CISCO_SPLIT_INC_1_MASKLEN="16"
	fi

	"$VPNC_SCRIPT" "$@"

	# Deserialize command
	OLDIFS="$IFS"
	IFS="\x1E"
	# shellcheck disable=SC2206  # splitting with custom IFS
	CMD=($COMMAND)
	IFS="$OLDIFS"

	# shellcheck disable=SC2154 # $reason is set by openconnect
	if [ -n "$NETNS_NAME" ] && [ -n "$RUNAS_USER" ] && [ "$reason" = "connect" ] ; then
		echo "Running requested command"
		cat <<- EOF | sudo -u "$RUNAS_USER" bash -s &
			#!/usr/bin/env bash
			${CMD[@]}
			echo
			echo "Command finished, press Ctrl-C to exit the network jail"
		EOF
	fi

	# openconnect waits for the vpnc-script to finish before start relaying traffic.
	exit
fi

# Read command line arguments:
mode="split"
while [ $# -gt 0 ]; do
	opt="$1"; shift
	case "$opt" in
		(-h|--help) usage; exit ;;
		(-f|--full) mode=full ;;
		(-j|--jail) mode=jail;
			RUNAS_USER="$1"
			if [ -z "$RUNAS_USER" ]; then
				die $E_USER "A user must be specified in jailed mode."
			fi
			shift
			if [ $# -lt 1 ]; then
				die $E_USER "A command to execute inside the jail must be specified."
			fi
			# Serialize the rest of the arguments, to be executed in bash inside the jail
			OLDIFS="$IFS"; IFS="\x1E"; COMMAND="$*"; IFS="$OLDIFS"
			break ;;
		(-*) die $E_USER 'Unknown option: %s' "$opt" ;;
		(*) die $E_USER 'Trailing argument: %s' "$opt" ;;
	esac
done
check_user
check_openconnect
check_vpnc_script

# Run in the right mode:
printf "Connecting in %s mode.\n" "$mode"
case "$mode" in
	(split) connect_split "$@" ;;
	(full) connect_full "$@" ;;
	(jail) connect_jailed ;;
esac
